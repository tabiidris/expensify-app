import  React from  'react';
import { BrowserRouter,Switch, Route} from "react-router-dom";

import AddExpense from './../componenets/AddExpense';
import EditExpense from './../componenets/EditExpense';
import Dashboard from './../componenets/Dashboard';
import Header from './../componenets/Header';
import HelpPage from './../componenets/Help';
import NotFoundPage from './../componenets/NotFound';


const AppRouter = () => (
    <BrowserRouter>
        <div>
            <Header />
            <Switch>
                <Route path="/" component={Dashboard} exact={true}/>
                <Route path="/create" component={AddExpense} />
                <Route path="/edit" component={EditExpense} />
                <Route path="/help" component={HelpPage} />
                <Route component={NotFoundPage} />
            </Switch>
        </div>
    </BrowserRouter>
);

export default AppRouter;