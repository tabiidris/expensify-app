// 
//Objects destructuring
//

//const person = {
//     name: 'Tabi Idris',
//     age: '25',
//     location: {
//         city: 'Buea',
//         temp: '36'
//     }
// }


// const {name, age, location} = person;

// console.log(`${name} is ${age} old`);

// const {city, temp} = location;

// console.log(`Recently the temperature has been above ${temp} in ${city}`);

//  const book = {
//      title: 'Ego is the Enermy',
//      author: 'Rhyan Holiday',
//      publisher:{
//         //  name: 'Penguin'
//      }
//  }

//  const {name:publisherName = 'Self-published'} = book.publisher;
 
//  console.log(publisherName);


//
//Array destructuring
//

const address = ['malingo street', 'Buea', 'SouthWest', 'Cameroon'];

const [, city, state = 'NorthWest'] = address;
 
console.log(`You are in ${city} ${state}`);

const item = []
