import {createStore} from 'redux';
import { type } from 'os';

const increaseCount  = ({increaseBy = 1} = {}) => ({
    type: 'INCREMENT',
    increaseBy
});

const decreaseCount = ({decreaseBy = 1} = {}) =>({
    type: 'DECREMENT',
    decreaseBy
});

const setCount = ({count}) => ({
    type: 'SET',
    count
});

const setReset = () =>({
    type: 'RESET'
});

const countReducer =  (state = {count: 0}, action)=>{

    switch(action.type){
        case 'INCREMENT':
            return{
                count: state.count + action.increaseBy
            };
        case 'DECREMENT':
            return{
                count: state.count - action.decreaseBy
            };
        case 'SET':
            return{
                count: action.count
            }
        case 'RESET':
            return{
                count: 0
            };
        default:
        return state;
    }
};                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          

const store = createStore(countReducer);

store.subscribe(()=>{
    console.log(store.getState());
});

store.dispatch(increaseCount({increaseBy:5}));
store.dispatch(increaseCount());
store.dispatch(setReset());
store.dispatch(decreaseCount());
store.dispatch(decreaseCount({decreaseBy: 10}));
store.dispatch(decreaseCount());
store.dispatch(setCount({count:102}));