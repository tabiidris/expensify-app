import {createStore, combineReducers} from 'redux';
import uuid from 'uuid';


//Add-expenses
const addExpense = (
    {
        description = '', 
        note = '', 
        amount = 0, 
        createdAt = 0 
    } = {}
) => ({
    type: 'ADD_EXPENSE',
    expense: {
        id: uuid(),
        description,
        note,
        amount,
        createdAt
    }
});


//Remove-expenses
const removeExpense = ({id}={}) => ({
    type: 'REMOVE_EXPENSE',
    id
});


//Edit-expenses
const editExpense = (id, update) => ({
    type: 'EDIT_EXPENSE',
    id,
    update
});

//Set-text-filters
const setTextFilter = (text = '') => ({
    type: 'SET_TEXT_FILTER',
    text
});

//Sort-by-date
const sortByDate = () => ({
    type: 'SORT_BY_DATE'
});

//Sort-by-amount
const sortByAmount = () => ({
    type: 'SORT_BY_AMOUNT'
});

//Set-start-date
const setStartDate = (startDate) => ({
    type: 'SET_START_DATE',
    startDate
});

//Set-end-date
const setEndDate = (endDate) => ({
    type: 'SET_END_DATE',
    endDate
});

//Expenses Reducer
const expensesReducerDefautState = [];
const expensesReducer = (state = expensesReducerDefautState, action ) => {
    switch(action.type){
        case 'ADD_EXPENSE':
            return [
                action.expense,
                ...state
            ];
        case 'REMOVE_EXPENSE':
            return state.filter(({id}) =>  id !== action.id);
        case 'EDIT_EXPENSE':
            return state.map((expense) => {
                if(expense.id === action.id){
                    return{
                        ...expense,
                        ...action.update
                    }
                }else{
                    return expense;
                }
            })
        default:
            return state;
    }
};

//Filters Reducer
const filtersReducerDefautState = {
    text: '', 
    sortBy: 'date',
    startDate: undefined,
    endDate: undefined
};
const filtersReducer = (state = filtersReducerDefautState, action ) => {
    switch(action.type){
        case 'SET_TEXT_FILTER':
            return {
                ...state,
                text: action.text
            };
        case 'SORT_BY_AMOUNT':
            return{
                ...state,
                sortBy: 'amount'
            };
        case 'SORT_BY_DATE':
            return{
                ...state,
                sortBy: 'date' 
            };
        case 'SET_START_DATE':
            return{
                ...state,
                startDate: action.startDate
            };
        case 'SET_END_DATE':
            return{
                ...state,
                endDate: action.endDate
            };
        default:
            return state;
    }
};

//get visible expenses
const getVisibleExpenses = (expenses, {text, sortBy, startDate, endDate}) => {
    return expenses.filter((expense) => {
        const startDateMatch = typeof startDate !== 'number' || expense.createdAt >= startDate;
        const endDateMatch = typeof endDate !== 'number' || expense.createdAt <= endDate;;
        const textMatch = expense.description.toLowerCase().includes(text.toLowerCase());

        return startDateMatch && endDateMatch && textMatch 
    });
};

//store creation
const store = createStore(
    combineReducers({
        expenses: expensesReducer,
        filters: filtersReducer
    })
);

store.subscribe(() => {
    const state = store.getState();
    const visibleExpenses = getVisibleExpenses(state.expenses, state.filters);
    console.log(visibleExpenses);
});

const expOne = store.dispatch(addExpense({
    description: 'Rent',
    amount: 100,
    createdAt: 1000
}));

const expTwo = store.dispatch(addExpense({
    description: 'Food',
    amount: 300,
    createdAt: -1000
}));

// store.dispatch(removeExpense({
//     id: expOne.expense.id
// }));

// store.dispatch(editExpense(
//     expTwo.expense.id, 
//     {
//         amount:5000
//     }
// ));

store.dispatch(setTextFilter('od'));
// store.dispatch(setTextFilter());

// store.dispatch(sortByAmount());
// store.dispatch(sortByDate());

// store.dispatch(setStartDate(2000));
// store.dispatch(setStartDate());

// store.dispatch(setEndDate());
// store.dispatch(setEndDate(5000));

const demoStore = {
     expenses: [{
         id: 'poijasdfhwer',
         description: 'Jenuary Rent',
         note: 'THis was the final payment for that address',
         amount: 45300,
         createdAt: 0
     }],
     filters: {
         text: 'rent',
         sortBy: 'amount', //date or amount
         startDate: undefined,
         endDate: undefined
     }
}
// const user ={
//     name: 'Tabi',
//     age: 25
// }

console.log({
    ...user
});
